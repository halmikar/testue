// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef THIRDPERSONDEBUG_ThirdPersonDebugGameMode_generated_h
#error "ThirdPersonDebugGameMode.generated.h already included, missing '#pragma once' in ThirdPersonDebugGameMode.h"
#endif
#define THIRDPERSONDEBUG_ThirdPersonDebugGameMode_generated_h

#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_RPC_WRAPPERS
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAThirdPersonDebugGameMode(); \
	friend THIRDPERSONDEBUG_API class UClass* Z_Construct_UClass_AThirdPersonDebugGameMode(); \
public: \
	DECLARE_CLASS(AThirdPersonDebugGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/ThirdPersonDebug"), THIRDPERSONDEBUG_API) \
	DECLARE_SERIALIZER(AThirdPersonDebugGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAThirdPersonDebugGameMode(); \
	friend THIRDPERSONDEBUG_API class UClass* Z_Construct_UClass_AThirdPersonDebugGameMode(); \
public: \
	DECLARE_CLASS(AThirdPersonDebugGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/ThirdPersonDebug"), THIRDPERSONDEBUG_API) \
	DECLARE_SERIALIZER(AThirdPersonDebugGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	THIRDPERSONDEBUG_API AThirdPersonDebugGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AThirdPersonDebugGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(THIRDPERSONDEBUG_API, AThirdPersonDebugGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AThirdPersonDebugGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	THIRDPERSONDEBUG_API AThirdPersonDebugGameMode(AThirdPersonDebugGameMode&&); \
	THIRDPERSONDEBUG_API AThirdPersonDebugGameMode(const AThirdPersonDebugGameMode&); \
public:


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	THIRDPERSONDEBUG_API AThirdPersonDebugGameMode(AThirdPersonDebugGameMode&&); \
	THIRDPERSONDEBUG_API AThirdPersonDebugGameMode(const AThirdPersonDebugGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(THIRDPERSONDEBUG_API, AThirdPersonDebugGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AThirdPersonDebugGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AThirdPersonDebugGameMode)


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_9_PROLOG
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_RPC_WRAPPERS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_INCLASS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_INCLASS_NO_PURE_DECLS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
