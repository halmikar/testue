// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef THIRDPERSONDEBUG_ThirdPersonDebugCharacter_generated_h
#error "ThirdPersonDebugCharacter.generated.h already included, missing '#pragma once' in ThirdPersonDebugCharacter.h"
#endif
#define THIRDPERSONDEBUG_ThirdPersonDebugCharacter_generated_h

#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_RPC_WRAPPERS
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAThirdPersonDebugCharacter(); \
	friend THIRDPERSONDEBUG_API class UClass* Z_Construct_UClass_AThirdPersonDebugCharacter(); \
public: \
	DECLARE_CLASS(AThirdPersonDebugCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ThirdPersonDebug"), NO_API) \
	DECLARE_SERIALIZER(AThirdPersonDebugCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAThirdPersonDebugCharacter(); \
	friend THIRDPERSONDEBUG_API class UClass* Z_Construct_UClass_AThirdPersonDebugCharacter(); \
public: \
	DECLARE_CLASS(AThirdPersonDebugCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ThirdPersonDebug"), NO_API) \
	DECLARE_SERIALIZER(AThirdPersonDebugCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AThirdPersonDebugCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AThirdPersonDebugCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AThirdPersonDebugCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AThirdPersonDebugCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AThirdPersonDebugCharacter(AThirdPersonDebugCharacter&&); \
	NO_API AThirdPersonDebugCharacter(const AThirdPersonDebugCharacter&); \
public:


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AThirdPersonDebugCharacter(AThirdPersonDebugCharacter&&); \
	NO_API AThirdPersonDebugCharacter(const AThirdPersonDebugCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AThirdPersonDebugCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AThirdPersonDebugCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AThirdPersonDebugCharacter)


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AThirdPersonDebugCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AThirdPersonDebugCharacter, FollowCamera); }


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_9_PROLOG
#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_RPC_WRAPPERS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_INCLASS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_INCLASS_NO_PURE_DECLS \
	ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ThirdPersonDebug_Source_ThirdPersonDebug_ThirdPersonDebugCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
